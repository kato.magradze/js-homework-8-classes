class Character {
    name = '';
    role = '';

    constructor(name, role) {
        this.name = name;
        this.role = role;
    }

    armor(arm) {
        console.log(`${this.name} Armor: ${arm}`);
    }

    damage(dmg) {
        console.log(`${this.name} Damage: ${dmg}`);
        console.log(`${this.name}: Attack Damage`);
    }
}

class Mage extends Character {
    magicPowers = '';

    constructor(name, role, magicPowers) {
        //super(name, 'Mage');
        super(name, role);
        this.magicPowers = magicPowers;
    }

    damage(dmg) {
        console.log(`${this.name} Damage: ${dmg}`);
        console.log(`${this.name}: Magic Damage`);
    }

    fly() {
        console.log(`${this.name} is a ${this.role} - it can fly!`);
    }
}

class Support extends Mage {
    heals = 0;

    constructor(name, magicPowers, heals) {
        super(name, 'Support', magicPowers);
        this.heals = heals;
    }

    healing() {
        console.log(`${this.name} can heal ${this.heals} units.`)
    }
}

class Adc extends Character {
    attackDamages = 0;

    constructor(name, attackDamages) {
        super(name, 'Adc');
        this.attackDamages = attackDamages;
    }

    checkRange(range) {
        if(range < 30) {
            console.log(`${this.name}'s Range: Close Range`);
        }
        else {
            console.log(`${this.name}'s Range: Far Range`);
        }
    }
}

let akali = new Character('Akali', 'Assassin');
// akali.armor(15);
// akali.damage(20);

let ahri = new Mage('Ahri', 'Mage', 'Fox-Fire');
// ahri.armor(5);
// ahri.damage(10);
// ahri.fly();
// console.log(ahri.magicPowers);

let thresh = new Support('Thresh', 'Damnation', 10);
// thresh.armor(10);
// thresh.damage(9);
// thresh.fly();
// thresh.healing();
// console.log(thresh.magicPowers);

let ashe = new Adc('Ashe', 32);
// ashe.armor(5);
// ashe.damage(16);
// ashe.checkRange(15);





